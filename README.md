# DevOpsCourse

#### Discente: Manuela Amélia Vieira de Azevedo Sodré 


##### Descrição da atividade:

Para o trabalho final você deverá criar um projeto que coloque em prática os conteúdos vistos no curso. 

##### Itens que devem ser cobertos:

- Criar um repositório Git no gitlab
- Criar automação com pipeline (ou afins)
- Criar imagens Docker, quando couber
- Deploy automático, quando couber.
- Uso do Kubernetes para deploy (desejável)

### Projeto:
# Wedesign_JS

Projeto inicialmente criado para fins de estudos da linguagem Javascript, e agora também para o projeto da disciplina de DEVOPS da residência em BI -Turma JF [IMD0190 - TÓPICOS ESPECIAIS EM BUSINESS INTELIGENCE E ANALYTICS 2]. 

O projeto em questão trata de uma página web que está sendo desenvolvida para fins de estudos da linguagem JavaScript. Nele criei uma pipeline de três estágios, um de build, um de teste e um de deploy. E para fins de estudo e avalição iclui um estágio de package para ver se conseguiria utilizar de uma imagem Docker para fins de estudo e avaliação. 
No estágio de package, para demonstração do docker, subi um servidor NGINX apenas para conter no projeto como seria a utilização e configuração de um dockerfile. 

 




